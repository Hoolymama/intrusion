import os.path
# import sys
import pymel.core as pm
import pymel.core.uitypes as gui


main_connectem_window = 'does_not_exist'


def unique_connectem_window():
    global main_connectem_window
    if pm.window(main_connectem_window, query=True, exists=True):
        pm.deleteUI(main_connectem_window)
    main_connectem_window = connectemWindow()


class connectemWindow(gui.Window):

    DEBUG = True

    def __init__(self):
        self.setTitle('Connect Em')
        self.setIconName('Connect Em')
        self.setWidthHeight([600, 600])


        self._form = pm.formLayout(numberOfDivisions=100)

        self._global_config_row = pm.rowColumnLayout(
                numberOfRows=1, rowHeight=(1, 24),
                ebg=True, bgc=(.4,.4,.4))
        self._global_config_buttons = {}
        self.create_global_config_buttons()
        pm.setParent(self._form)


        self._left_config_row = pm.rowColumnLayout(
                numberOfRows=1, rowHeight=(1, 24),
                ebg=True, bgc=(.4,.4,.6))
        self.create_config_buttons('left')
        pm.setParent(self._form)
        self._left_outliner = self.create_outliner(side='left')
        pm.setParent(self._form)
        self._left_attr_spec_col = self.create_attr_spec(side='left')
        pm.setParent(self._form)



        self._right_config_row = pm.rowColumnLayout(
                numberOfRows=1, rowHeight=(1, 24),
                ebg=True, bgc=(.4,.6,.4))
        self.create_config_buttons('right')
        pm.setParent(self._form)
        self._right_outliner = self.create_outliner(side='right')
        pm.setParent(self._form)
        self._right_attr_spec_col = self.create_attr_spec(side='right')
        pm.setParent(self._form)

        pm.setParent(self._form)
        self.create_buttons()
        self.show()
        self.populate_both_from_sel()
        self.setResizeToFitChildren()


    def populate_side(self, objects, side):
        outliner =  self._left_outliner
        if side == 'right':
            outliner =  self._right_outliner
        for obj in objects:
            pm.textScrollList(outliner, edit=True, append=obj)
        self.on_change_outliner_list(side)

    def populate_both_from_sel(self):
        sel = pm.ls(selection=True)
        self.populate_side(sel, 'left')
        self.populate_side(sel, 'right')
        
    # def populate_option_menu(self):

    def on_change_outliner_list(self, side):
        outliner =  self._left_outliner
        if side == 'right':
            outliner =  self._right_outliner

        node = None
        nodes = pm.textScrollList(outliner, query=True, selectItem=True)
        if nodes and len(nodes) > 0:
            node = nodes[0]
        else:
            nodes = pm.textScrollList(outliner, query=True, allItems=True)
            if nodes and len(nodes) > 0:
                node = nodes[0]

        # if node is not None:
        print 'NODE %s' % node

        # pm.setParent(self.tabs)
        # if 'connectemPackages' in pm.env.optionVars:
        # #     self.browse_for_family()
        # # else:
        #     current_package = None
        #     if 'connectemCurrentTab' in pm.env.optionVars:
        #         current_package = pm.optionVar['connectemCurrentTab']

        #     ov = pm.optionVar['connectemPackages']
        #     if not isinstance(ov, pm.language.OptionVarList):
        #         ov = [ov]

        #     select_tab_id = 0
        #     curr = 1
        #     for package_name in ov:
        #         if self.register_family(package_name):
        #             if package_name == current_package:
        #                 select_tab_id = curr
        #             curr += 1
        #     if select_tab_id > 0:
        #         self.tabs.setSelectTabIndex(select_tab_id)
        #     self.sync_connectem_packages_option_var()
        #     self.set_families_to_current_config_state()

    def create_outliner(self, side='src'):
        
        outliner = pm.textScrollList(
            allowMultiSelection=True,
            deleteKeyCommand=pm.Callback(self.on_deselect_click, 'both')
            )

        # outliner = pm.nodeOutliner(
        #     showInputs=False,
        #     showOutputs=False,
        #     multiSelect=True
        # )

        return outliner

    def create_config_buttons(self, side):
        # parent = 
        # pm.setParent(self._left_config_row)
        pm.symbolButton(
            image="setEdAddCmd.xpm",
            height=24, width=24,
            command=pm.Callback(self.on_add_selection_click, side)
            )
            
        pm.symbolButton(
            image="setEdRemoveCmd.xpm",
            height=24, width=24,
            command=pm.Callback(self.on_delete_selection_click, side)
            )

        pm.symbolButton(
            image="setEdSelectMode.xpm",
            height=24, width=24,
            command=pm.Callback(self.on_deselect_click, side)
            )
        # pm.iconTextCheckBox(
        #     style='iconAndTextVertical', width=24,
        #     image='lock.png',
        #     ann='Lock the list'
        #     )
      
    # def create_right_config_buttons(self):
    #     pm.setParent(self._right_config_row)
    #     pm.symbolButton(
    #         image="setEdAddCmd.xpm",
    #         height=24, width=24)
    #     pm.symbolButton(
    #         image="setEdRemoveCmd.xpm",
    #         height=24, width=24)


    def create_attr_spec(self, side):
        col = pm.columnLayout(adj=True)
        pm.optionMenuGrp(label='Attribute', cw2=(50,150), adj=2)
        pm.textFieldGrp(label='Template', cw2=(50,150), adj=2)
        if side == 'left':
            pm.intFieldGrp(label='Index', cw2=(50,150), adj=2)
        else:
            pm.rowLayout(nc=2)
            pm.intFieldGrp(label='Index', cw2=(50,150), adj=2)
            pm.checkBox(label='NA')
        return col

    def create_global_config_buttons(self):
        pm.setParent(self._global_config_row)
        self._global_config_buttons['verbose'] = pm.iconTextRadioCollection()
        pm.iconTextRadioButton(
            style='iconOnly', width=24,
            image='connectem_verbose1_off.png',
            selectionImage='connectem_verbose1_on.png',
            ann='verbose level 1',
            onCommand=pm.Callback(self.on_verbosity_button_click, 1)
            )
        pm.iconTextRadioButton(
            style='iconOnly', width=24,
            image='connectem_verbose2_off.png',
            selectionImage='connectem_verbose2_on.png',
            ann='verbose level 2',
            onCommand=pm.Callback(self.on_verbosity_button_click, 2)
            )
        pm.iconTextRadioButton(
            style='iconOnly', width=24,
            image='connectem_verbose3_off.png',
            selectionImage='connectem_verbose3_on.png',
            ann='verbose level 3',
            onCommand=pm.Callback(self.on_verbosity_button_click, 3)
            )
        pm.setParent(self._global_config_row)
        pm.separator(style='double', horizontal=False)
        self._global_config_buttons['debug'] = pm.symbolCheckBox(
            value=False,
            width=24,
            onImage='connectem_dbg_on.png',
            offImage='connectem_dbg_off.png',
            onCommand=pm.Callback(self.on_debug_button_click, True),
            offCommand=pm.Callback(self.on_debug_button_click, False)
            )
        pm.symbolButton(
            image="setEdSelectMode.xpm",
            height=24, width=24,
            command=pm.Callback(self.on_deselect_click, 'both')
            )
        # pm.setParent(self._form)

    def create_buttons(self):
        pm.setParent(self._form)
        b1 = pm.button(
            label='Connect',
            command=pm.Callback(self.on_connect_click)
            )
        b2 = pm.button(
            label='Dry Run',
            command=pm.Callback(self.on_dry_run_click)
            )
        b3 = pm.button(
            label='Close',
            command=('connectem.pm.deleteUI(\"' + self + '\", window=True)')
            )
        pm.formLayout(
            self._form,
            edit=True,
            attachControl=[
                (self._left_config_row, 'top', 2, self._global_config_row),
                (self._right_config_row, 'top', 2, self._global_config_row),
                (self._left_outliner, 'top', 2, self._left_config_row),
                (self._right_outliner, 'top', 2, self._right_config_row),
                (self._left_outliner, 'bottom', 2, self._left_attr_spec_col),
                (self._right_outliner, 'bottom', 2, self._right_attr_spec_col),
                (self._left_attr_spec_col, 'bottom', 2, b1),
                (self._right_attr_spec_col, 'bottom', 2, b3)
                ],
            attachPosition=[
                (self._left_config_row, 'right', 2, 50),
                (self._right_config_row, 'left', 2, 50),
                (self._left_outliner, 'right', 2, 50),
                (self._right_outliner, 'left', 2, 50),
                (self._left_attr_spec_col, 'right', 2, 50),
                (self._right_attr_spec_col, 'left', 2, 50),
                (b1, 'right', 2, 33), (b2, 'left', 2, 33),
                (b2, 'right', 2, 66), (b3, 'left', 2, 66)
                ],
            attachNone=[
                (b3, 'top'), (b2, 'top'), (b1, 'top'),
                (self._left_attr_spec_col, 'top'),
                (self._right_attr_spec_col, 'top'),
                (self._right_config_row, 'bottom'),
                (self._left_config_row, 'bottom')
                ],
            attachForm=[
                (b1, 'left', 2),
                (b1, 'bottom', 2),
                (b2, 'bottom', 2),
                (b3, 'bottom', 2),
                (b3, 'right', 2),
                (self._right_config_row, 'right', 2),
                (self._left_config_row, 'left', 2),
                (self._right_outliner, 'right', 2),
                (self._left_outliner, 'left', 2),
                (self._global_config_row, 'top', 2),
                (self._global_config_row, 'right', 2),
                (self._global_config_row, 'left', 2),
                (self._left_attr_spec_col, 'left', 2),
                (self._right_attr_spec_col, 'right', 2)
                ]
            )


    # def initialize_config_buttons(self):
    #     verbosity = 1
    #     if 'connectemVerbosity' in pm.env.optionVars:
    #         verbosity = su.clamp(pm.optionVar['connectemVerbosity'], 1, 3)
    #     index = verbosity - 1
    #     buttons = self.config_buttons['verbose'].getCollectionItemArray()
    #     self.config_buttons['verbose'].setSelect(buttons[index])
    #     dbg = False
    #     if 'connectemDebug' in pm.env.optionVars:
    #         dbg = pm.optionVar['connectemDebug']
    #         dbg = (False, True)[dbg]
    #     self.config_buttons['debug'].setValue(dbg)

    # def set_families_to_current_config_state(self):
    #     if not 'connectemVerbosity' in pm.env.optionVars:
    #         pm.optionVar['connectemVerbosity'] = 1
    #     verbosity = pm.optionVar['connectemVerbosity']
    #     if not 'connectemDebug' in pm.env.optionVars:
    #         pm.optionVar['connectemDebug'] = False
    #     dbg = pm.optionVar['connectemDebug']
    #     dbg = (False, True)[dbg]
    #     self.set_familiy_verbosity(verbosity)
    #     self.set_familiy_debug(dbg)

    def on_verbosity_button_click(self, value):
        pm.optionVar['connectemVerbosity'] = value
        print "connectem verbosity %d" % value
        # self.set_familiy_verbosity(value)

    def on_debug_button_click(self, value):
        pm.optionVar['connectemDebug'] = value
        print "connectem debug %s" % value
    
    def on_connect_click(self):
        # pm.optionVar['connectemDebug'] = value
        print "Do ConnectEm"
           
    def on_dry_run_click(self):
        # pm.optionVar['connectemDebug'] = value
        print "Do Dry Run"

    def on_add_selection_click(self, side):
        outliner = self._left_outliner
        if (side == 'right'):
            outliner = self._right_outliner
        sel = pm.ls(selection=True)
        all_items = pm.textScrollList(outliner, query=True, allItems=True)
        for node in sel:
            if node not in all_items:
                pm.textScrollList(outliner, edit=True, append=node)

        self.on_change_outliner_list(side)
        # print "Add Sel %s" % side

    def on_delete_selection_click(self, side):
        outliner = self._left_outliner
        if (side == 'right'):
            outliner = self._right_outliner
        nodes = pm.textScrollList(outliner, query=True, selectItem=True)

        if nodes is None or (len(nodes) == 0):
            pm.textScrollList(outliner, edit=True, removeAll=True)
        else:
            for node in nodes:
                pm.textScrollList(outliner, edit=True, removeItem=node)
        self.on_change_outliner_list(side)

    def on_deselect_click(self, side):
        if side == 'left' or side == 'both':
            pm.textScrollList(self._left_outliner, edit=True, deselectAll=True)
        if side == 'right' or side == 'both':
            pm.textScrollList(self._right_outliner, edit=True, deselectAll=True)


        # print "Delete Sel %s" % side

        # self.set_familiy_debug(value)

    # def set_familiy_verbosity(self, value):
    #     for family in self.families:
    #         for check_ui in family.get_check_uis():
    #             check_ui.set_verbosity(value)
    #         family.get_aggregator_ui().set_verbosity(value)

    # def set_familiy_debug(self, value):
    #     for family in self.families:
    #         for check_ui in family.get_check_uis():
    #             check_ui.connectem_check.set_debug(value)
    #         family.get_aggregator_ui().connectem_check.set_debug(value)
    #     self.DEBUG = value



    # based on the current tab index, we want to
    # get the family package name and save its value in
    # the connectemCurrentTab option var
    # def on_tab_changed(self):
    #     tab_id = self.tabs.getSelectTabIndex()
    #     if tab_id > 0:
    #         pm.optionVar['connectemCurrentTab'] = self.families[(
    #             tab_id - 1)].package_name

    # # set the optionVar to the list
    # # of family package names
    # def sync_connectem_packages_option_var(self):
    #     pm.optionVar.pop('connectemPackages')
    #     if self.families:
    #         names = []
    #         for f in self.families:
    #             names.append(f.package_name)
    #         pm.optionVar['connectemPackages'] = names

    # on opening the window, add a tab for each
    # family stored in the option var. If no
    # saved families, open the file dialog

    # remove a family and its UI, then sync optionVar
    # def delete_current_family(self):
    #     tab_id = self.tabs.getSelectTabIndex()
    #     if tab_id == 0:
    #         pm.displayWarning('No tabs to delete')
    #     else:
    #         index = tab_id - 1
    #         num_c = self.tabs.getNumberOfChildren()
    #         num_f = len(self.families)
    #         if not num_f == num_c:
    #             pm.displayError('num_f num_c dont match')
    #             return
    #         pm.deleteUI(self.families.pop(index).column)
    #         self.sync_connectem_packages_option_var()

    # browse for a package containing a family of connectem checks.
    # try to register
    # def browse_for_family(self):
    #     d = os.path.join(os.path.dirname(os.path.dirname(
    #         os.path.abspath(__file__))), 'family')

    #     #d = pm.workspace.getPath()
    #     cap = 'Import a connectem python package'
    #     ok = 'Import'
    #     entries = pm.fileDialog2(caption=cap, okCaption=ok, fileFilter='*.py',
    #                              dialogStyle=2, fileMode=3, dir=d)
    #     if not entries:
    #         pm.displayWarning('Nothing Selected')
    #         return
    #     if self.register_family(os.path.basename(entries[0])):
    #         self.sync_connectem_packages_option_var()
    #         self.set_families_to_current_config_state()

    # def package_registered(self, package_name):
    #     for f in self.families:
    #         if f.package_name == package_name:
    #             return True
    #     return False

    # def register_family(self, package_name):

    #     if not self.package_registered(package_name):
    #         if not self.DEBUG:
    #             try:
    #                 family = sfm.Family(package_name, self.tabs)
    #                 self.families.append(family)
    #                 return True
    #             except ImportError:
    #                 pm.displayWarning(package_name + ' is not a valid package')
    #                 return False
    #         else:
    #             family = sfm.Family(package_name, self.tabs)
    #             self.families.append(family)
    #             return True
    #     else:
    #         pm.displayWarning(package_name + ' is already registered')
