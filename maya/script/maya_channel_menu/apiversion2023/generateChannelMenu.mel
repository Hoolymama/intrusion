// ===========================================================================
// Copyright 2021 Autodesk, Inc. All rights reserved.
//
// Use of this software is subject to the terms of the Autodesk license
// agreement provided at the time of installation or download, or which
// otherwise accompanies this software in either electronic or hard copy form.
// ===========================================================================
global proc enableDisableChannelMenu( string $parent )
//
// Enables or disables menu items in the channelBox
// menus, based on whether channels are selected or
// not.
{
	int $channelsSelected = `selectedChannels`;

	menuItem -e -enable $channelsSelected ( $parent + "|keyItem" );
	menuItem -e -enable $channelsSelected ( $parent + "|breakdownItem" );
	menuItem -e -enable $channelsSelected ( $parent + "|muteItem" );
	menuItem -e -enable $channelsSelected ( $parent + "|unmuteItem" );		
	menuItem -e -enable $channelsSelected ( $parent + "|copyItem" );
	menuItem -e -enable $channelsSelected ( $parent + "|cutItem" );
	menuItem -e -enable $channelsSelected ( $parent + "|pasteItem" );
	menuItem -e -enable $channelsSelected ( $parent + "|deleteItem" );
	menuItem -e -enable $channelsSelected ( $parent + "|duplicateAttrValueItem" );
	menuItem -e -enable $channelsSelected ( $parent + "|lockItem" );
	menuItem -e -enable $channelsSelected ( $parent + "|unlockItem" );
	menuItem -e -enable $channelsSelected ( $parent + "|unkeyableItem" );
	menuItem -e -enable $channelsSelected ( $parent + "|lockUnkeyableItem" );
	menuItem -e -enable $channelsSelected ( $parent + "|unkeyableDisplayedItem" );
	menuItem -e -enable $channelsSelected ( $parent + "|keyableItem" );

	menuItem -e -enable $channelsSelected ( $parent + "|breakItem" );
	menuItem -e -enable $channelsSelected ( $parent + "|selectConnectionItem" );	

	int $sync = `optionVar -query keyUseChannelBox`;
	menuItem -e -cb $sync cbGraphEdSyncMenu;

	global string $gPlayBackSlider;
	int $tcDisplay = `timeControl -q -showKeysCombined $gPlayBackSlider`;
	menuItem -e -cb $tcDisplay cbTimelineSyncMenu;
}

global proc
toggleChannelBoxGraphEdSync(int $enable)
{
	global int $gChannelFcurveEdSyncCallback;

	optionVar -intValue keyUseChannelBox $enable;

	if ($enable == 0 && $gChannelFcurveEdSyncCallback > 0) {
		scriptJob -k $gChannelFcurveEdSyncCallback;
		$gChannelFcurveEdSyncCallback = -1;
	}
	if ($enable) {
		optionVar -category "Animation.Key.Set Key" -iv setKeyframeWhichBackup `optionVar -q setKeyframeWhich`;
		optionVar -intValue setKeyframeWhich 1;
		if ($gChannelFcurveEdSyncCallback < 0) {
			// The same code exists in the main code of initChannelBox.mel
			$gChannelFcurveEdSyncCallback = `scriptJob -event "ChannelBoxLabelSelected" syncChannelBoxFcurveEd`;
		}
	} else {
		if (! `optionVar -exists setKeyframeWhichBackup`) {
			optionVar -intValue setKeyframeWhich 4; // factory default
		} else {
			optionVar -intValue setKeyframeWhich `optionVar -q setKeyframeWhichBackup`;
		}
	}
}


global proc
toggleChannelBoxTimelineSync(int $enable)
{
	global string $gChannelBoxName;
	global string $gPlayBackSlider;	
	
	if ($enable) {
		string $syncTimeline = `timeControl -q -showKeys $gPlayBackSlider`;
		timeControl -e -showKeys $gChannelBoxName $gPlayBackSlider;
		timeControl -e -showKeysCombined 1 $gPlayBackSlider;
	} else {
		timeControl -e -showKeysCombined 0 $gPlayBackSlider;
		timeControl -e -showKeys "active" $gPlayBackSlider;
	}
}

global proc generateChannelMenu( string $parent, int $popupMenu )
//
// Generates the menu on both the menubar and on
// the RMB, when the user asks for either of these.
//
{
	setParent -m $parent;

	if( 0 != `menu -q -ni $parent` ) {
		//
		// The menu already exists - just set the
		// enable/disable state for
		// each of the menuItems that operation on
		// selected channels.
		//
		enableDisableChannelMenu $parent;

	} else {
		//
		// The menu doesn't exist yet - build it
		//
		if( $popupMenu ) {
			//
			//	A popup menu is being generated - attach
			//	a label to the top of the popup
			//
			menuItem -label (uiRes("m_generateChannelMenu.kChannels"));
			menuItem -d true;
			menuItem -d true;
		}
		menuItem -label (uiRes("m_generateChannelMenu.kKeySelected")) -c "channelBoxCommand -key" keyItem;
		menuItem -label (uiRes("m_generateChannelMenu.kKeyAll")) -c "channelBoxCommand -keyall" keyAllItem;
		menuItem -label (uiRes("m_generateChannelMenu.kBreakDownSelected")) -c "channelBoxCommand -breakdown" breakdownItem;
		menuItem -label (uiRes("m_generateChannelMenu.kBreakDownAll")) -c "channelBoxCommand -breakdownall" breakdownAllItem;
		menuItem -label (uiRes("m_generateChannelMenu.kMuteSelected")) -c "channelBoxCommand -mute" muteItem;
		menuItem -label (uiRes("m_generateChannelMenu.kMuteAll")) -c "channelBoxCommand -muteall" muteAllItem;
		menuItem -label (uiRes("m_generateChannelMenu.kUnmuteSelected")) -c "channelBoxCommand -unmute" unmuteItem;
		menuItem -label (uiRes("m_generateChannelMenu.kUnmuteAll")) -c "channelBoxCommand -unmuteall" unmuteAllItem;

		menuItem -divider true;
		
		menuItem -label (uiRes("m_generateChannelMenu.kSyncGraphEditor"))
			-cb `optionVar -query keyUseChannelBox`
			-annotation (uiRes("m_generateChannelMenu.kSyncGraphEditorAnnot"))
			-c "toggleChannelBoxGraphEdSync #1" cbGraphEdSyncMenu;

		int $tcDisplay = `timeControl -q -showKeysCombined timeControl1`;
		menuItem -label (uiRes("m_generateChannelMenu.kSyncTimeline"))
			-cb $tcDisplay -c "toggleChannelBoxTimelineSync #1"
			-annotation (uiRes("m_generateChannelMenu.kSyncTimelineAnnot"))
			cbTimelineSyncMenu;

		menuItem -divider true;

		menuItem -label (uiRes("m_generateChannelMenu.kCutSelected")) -c "channelBoxCommand -cut" cutItem;
		menuItem -label (uiRes("m_generateChannelMenu.kCopySelected")) -c "channelBoxCommand -copy" copyItem;
		menuItem -label (uiRes("m_generateChannelMenu.kPasteSelected")) -c "channelBoxCommand -paste" pasteItem;
		menuItem -label (uiRes("m_generateChannelMenu.kDeleteSelected")) -c "channelBoxCommand -delete" deleteItem;

		menuItem -d true;
		
		menuItem -label (uiRes("m_generateChannelMenu.kDuplicateValues")) -c "copyAttrValues" duplicateAttrValueItem;
		menuItem -label (uiRes("m_generateChannelMenu.kFreeze")) -subMenu true;
			menuItem -label (uiRes("m_generateChannelMenu.kTranslate")) -c "channelBoxCommand -freezeTranslate" freezeTranslateItem;
			menuItem -label (uiRes("m_generateChannelMenu.kRotate")) -c "channelBoxCommand -freezeRotate" freezeRotateItem;
			menuItem -label (uiRes("m_generateChannelMenu.kScale")) -c "channelBoxCommand -freezeScale" freezeScaleItem;
			menuItem -label (uiRes("m_generateChannelMenu.kAll")) -c "channelBoxCommand -freezeAll" freezeAllItem;
		setParent -m ..;
		
		menuItem -d true;

		menuItem -label (uiRes("m_generateChannelMenu.kBreakConnections")) -c "channelBoxCommand -break" breakItem;
		menuItem -label (uiRes("m_generateChannelMenu.kSelectConnection")) -c "channelBoxCommand -selectConnection" selectConnectionItem;

		menuItem -d true;

		menuItem -label (uiRes("m_generateChannelMenu.kLockSelected")) -c "channelBoxCommand -lock" lockItem;
		menuItem -label (uiRes("m_generateChannelMenu.kUnlockSelected")) -c "channelBoxCommand -unlock" unlockItem;
		menuItem -label (uiRes("m_generateChannelMenu.kHideSelected")) -c "channelBoxCommand -unkeyable" unkeyableItem;
		menuItem -label (uiRes("m_generateChannelMenu.kLockHideSelected")) -c "channelBoxCommand -lockUnkeyable" lockUnkeyableItem;
		menuItem -label (uiRes("m_generateChannelMenu.kMakeSelectedNonkeyable")) -c "channelBoxCommand -unkeyableDisplayed" unkeyableDisplayedItem;
		menuItem -label (uiRes("m_generateChannelMenu.kMakeSelectedKeyable")) -c "channelBoxCommand -keyable" keyableItem;		
		
		menuItem -d true;
		menuItem -label (uiRes("m_generateChannelMenu.kAddToSelectedLayers")) -c "channelBoxCommand -addToLayers" addToLayersItem;
		menuItem -label (uiRes("m_generateChannelMenu.kRemoveFromSelectedLayers")) -c "channelBoxCommand -removeFromLayers" removeFromLayersItem;
		
		enableDisableChannelMenu $parent;
		print("Appending custom items to Channel Menu...\n");
		catch(`channelMenuAppendCustomItems $parent`);
	}
}

global proc buildChannelBoxOptionMenu( string $parent ) {
//
//	Builds the menu containing the options for various
//	settings in the channel box.
//
	
	setParent -m $parent;
	menu -e -dai $parent;

    radioMenuItemCollection;
	menuItem -label (uiRes("m_generateChannelMenu.kSlowest"))
		-radioButton (0.01 == `channelBox -q -speed mainChannelBox`)
		-c "channelBoxSettings slowest #1";
    menuItem -label (uiRes("m_generateChannelMenu.kSlow")) 
		-radioButton (0.1 == `channelBox -q -speed mainChannelBox`)
		-c "channelBoxSettings slow #1";
	menuItem -label (uiRes("m_generateChannelMenu.kMedium")) 
		-radioButton (1.0 == `channelBox -q -speed mainChannelBox`)
		-c "channelBoxSettings medium #1";
	menuItem -label (uiRes("m_generateChannelMenu.kFast")) 
		-radioButton (10.0 == `channelBox -q -speed mainChannelBox`)
		-c "channelBoxSettings fast #1";
	menuItem -d true;
	menuItem -label (uiRes("m_generateChannelMenu.kHyperbolic")) 
		-checkBox `channelBox -q -hyperbolic mainChannelBox`
		-c "channelBoxSettings hyperbolic #1";
	menuItem -d true;
	menuItem -label (uiRes("m_generateChannelMenu.kShowNamespace")) 
		-checkBox `channelBox -q -showNamespace mainChannelBox`
		-c "channelBoxSettings showNamespace #1";
	menuItem -d true;

	radioMenuItemCollection;
	menuItem -label (uiRes("m_generateChannelMenu.kNoManips")) 
		-annotation (uiRes("m_generateChannelMenu.kNoManipsAnot"))
		-radioButton ("none" == `channelBox -q -useManips mainChannelBox` )
		-c "channelBoxSettings useManips 0";
	menuItem -label (uiRes("m_generateChannelMenu.kInvisibleManips")) 
		-annotation (uiRes("m_generateChannelMenu.kInvisManipsAnot"))
		-radioButton ("invisible" == `channelBox -q -useManips mainChannelBox` )
		-c "channelBoxSettings useManips 1";
	menuItem -label (uiRes("m_generateChannelMenu.kStandardManips")) 
		-annotation (uiRes("m_generateChannelMenu.kStandardManipsAnot"))
		-radioButton ("standard" == `channelBox -q -useManips mainChannelBox` )
		-c "channelBoxSettings useManips 2";
	menuItem -d true;

	menuItem -label (uiRes("m_generateChannelMenu.kChangePrecision"))
		-command "channelBoxCommand -precision";

	menuItem -label (uiRes("m_generateChannelMenu.kResetDefault")) 
		-command "channelBoxCommand -reset";
}
