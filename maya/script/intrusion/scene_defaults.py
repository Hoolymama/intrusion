import pymel.core as pm


def convert_timeunit_to_fps(name):
    opts = {'game': 15, 'film': 24, 'pal': 25, 
        'ntsc': 30, 'show': 48, 'palf': 50, 'ntscf': 60}
    if name in opts:
        return opts[name]


def on_new_scene(*args, **kwargs):
    
    #offset is frames, duration is seconds
    offset = kwargs.get('offset')
    duration = kwargs.get('duration')
    time_unit = kwargs.get('time_unit')
    angular_unit = kwargs.get('angular_unit')
    linear_unit = kwargs.get('linear_unit')

    if time_unit:
        pm.currentUnit(time=time_unit)

    time_unit =  pm.currentUnit(query=True, time=True)

    if angular_unit:
        pm.currentUnit(angle=angular_unit)

    if linear_unit:
        pm.currentUnit(linear=linear_unit)

    if offset or (offset == 0):
        pm.playbackOptions(edit=True, ast=offset, min=(offset+1))
        if not duration:
            duration = 1

    if duration or (duration == 0):
        start = pm.playbackOptions(q=True,  min=True)
        end_frame = start + (duration * convert_timeunit_to_fps(time_unit))
        pm.playbackOptions(edit=True, aet=end_frame, max=end_frame)

    # now deal with args
    for arg in args:
        if not isinstance(arg, tuple):
            continue
        if not len(arg) == 2:
            continue
        try:
            pm.Attribute(arg[0]).set(arg[1])
        except:
            pm.warning("Problem setting %s to %s" % arg)

    pm.cycleCheck(all=False)

def on_check_scene(*args, **kwargs):
    offset = kwargs.get('offset')
    time_unit = kwargs.get('time_unit')
    angular_unit = kwargs.get('angular_unit')
    linear_unit = kwargs.get('linear_unit')
    decoration = ">>>"

    if time_unit:
        curr = pm.currentUnit(query=True, time=True)
        if time_unit != curr:
            pm.warning("%s Scene time unit is %s but project time unit is %s" % (decoration, curr, time_unit))

    if angular_unit:
        curr = pm.currentUnit(query=True, angle=True)
        if angular_unit != curr:
            pm.warning("%s Scene angular unit is %s but project angular unit is %s" % (decoration, curr, angular_unit))
    
    if linear_unit:
        curr = pm.currentUnit(query=True, linear=True)
        if linear_unit != curr:
            pm.warning("%s Scene linear unit is %s but project linear unit is %s" % (decoration, curr, linear_unit))

    if offset:
        curr = pm.playbackOptions(query=True,  min=True)
        if not offset == curr-1:
            pm.warning("%s Scene time offset is %s but project time offset is %s" % (decoration, (curr - 1), offset))


    # now deal with args
    for arg in args:
        if not isinstance(arg, tuple):
            continue
        if not len(arg) == 2:
            continue
        try:
            val = pm.Attribute(arg[0]).get()
            if val != arg[1]:
                pm.warning("%s %s is %s but project specifies %s" % (decoration, arg[0], val, arg[1]))
        except:
            pm.warning("Problem checking value of %s" % arg[0])

def do_scene_setup(*args, **kwargs):

    new_scene_callback = pm.Callback(on_new_scene, *args,  **kwargs)
    check_scene_callback = pm.Callback(on_check_scene, *args, **kwargs)

    pm.scriptJob(protected=True, event=["NewSceneOpened", new_scene_callback])
    pm.scriptJob(protected=True, event=["SceneOpened", check_scene_callback])

    pm.executeDeferred(new_scene_callback)
    