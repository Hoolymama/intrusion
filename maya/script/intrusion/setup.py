import pymel.core as pm
import os 

def main():
    # Channel Box
    # pm.mel.source("generateChannelMenu", language="mel")
    # pm.mel.source("channelBoxConnect", language="mel")
    # pm.mel.source("channelBoxEnumerator", language="mel")
    # pm.mel.source("channelBoxInsertMathNodeMenuItems", language="mel")
    # pm.mel.source("channelBoxInsertNoise", language="mel")
    # pm.mel.source("channelBoxPrintStats", language="mel")
    # pm.mel.source("channelBoxTimeDerivative", language="mel")
    # pm.mel.source("channelBoxTimeDerivative", language="mel")
    # pm.mel.source("channelMenuAppendCustomItems", language="mel")
    # pm.mel.source("defaultOpsChannelMenu", language="mel")
    
    fn = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))) ,"generateChannelMenu.mel" )
    pm.mel.source(fn, language="mel")
 
    # Commands: Hotkeys
    pm.mel.source("rtcToggle", language="mel")
    pm.mel.source("rtcCameraNavigation", language="mel")
    pm.mel.source("rtcCycleSelection", language="mel")
    pm.mel.source("rtcConnectWindow", language="mel")
    pm.mel.source("rtcMisc", language="mel")
    pm.mel.source("rtcMesh", language="mel")
    pm.mel.source("rtcNodeEditor", language="mel")
    pm.mel.source("rtcRebuildAE", language="mel")



    pm.mel.rtcToggleCommands("Intrusion")
    pm.mel.rtcCameraNavigationCommands("Intrusion")
    pm.mel.rtcCycleSelectionCommands("Intrusion")
    pm.mel.rtcConnectWindowCommands("Intrusion")
    pm.mel.rtcMiscCommands("Intrusion")
    pm.mel.rtcMeshCommands("Intrusion")
    pm.mel.rtcNodeEditorCommands("Intrusion")
    pm.mel.rtcRebuildAECommands("Intrusion")